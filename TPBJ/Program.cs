﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPBJ
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            OpenFileDialog openFileDialog = new OpenFileDialog();

            while (!File.Exists(Properties.Settings.Default.IRCPath))
            {
                openFileDialog.InitialDirectory = Properties.Settings.Default.IRCPath;
                openFileDialog.Title = "Select the HexChat log file";
                openFileDialog.Filter = "log files (*.log)|*.log|All files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(openFileDialog.FileName))
                    {
                        Properties.Settings.Default.IRCPath = openFileDialog.FileName;
                        Properties.Settings.Default.Save();
                    }
                }
            }

            Application.Run(new Form1());
        }
    }
}
