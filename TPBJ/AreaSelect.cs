﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPBJ
{
    public partial class AreaSelect : Form
    {
        public AreaSelect()
        {
            InitializeComponent();
        }

        private void AreaSelect_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1.MainForm.size = new Size(Width, Height);
            Form1.MainForm.origin = new Point(Left, Top);
            Properties.Settings.Default.Size = new Size(Width, Height);
            Properties.Settings.Default.Origin = new Point(Left, Top);
            Properties.Settings.Default.Save();

            Form1.MainForm.updateGame();
        }
    }
}