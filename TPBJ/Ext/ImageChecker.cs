﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPBJ.Ext
{
    //class LockedFastImage
    //{
    //    private Bitmap image;
    //    private byte[] rgbValues;
    //    private System.Drawing.Imaging.BitmapData bmpData;

    //    private IntPtr ptr;
    //    private int bytes;

    //    public LockedFastImage(Bitmap image)
    //    {
    //        this.image = image;
    //        Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);
    //        bmpData = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, image.PixelFormat);

    //        ptr = bmpData.Scan0;
    //        bytes = Math.Abs(bmpData.Stride) * image.Height;
    //        rgbValues = new byte[bytes];
    //        System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
    //    }

    //    ~LockedFastImage()
    //    {
    //        // Try to unlock the bits. Who cares if it dont work...
    //        try
    //        {
    //            image.UnlockBits(bmpData);
    //        }
    //        catch { }
    //    }

    //    /// <summary>
    //    /// Returns or sets a pixel of the image. 
    //    /// </summary>
    //    /// <param name="x">x parameter of the pixel</param>
    //    /// <param name="y">y parameter of the pixel</param>
    //    public Color this[int x, int y]
    //    {
    //        get
    //        {
    //            int index = (x + (y * image.Width)) * 4;
    //            return Color.FromArgb(rgbValues[index + 3], rgbValues[index + 2], rgbValues[index + 1], rgbValues[index]);
    //        }

    //        set
    //        {
    //            int index = (x + (y * image.Width)) * 4;
    //            rgbValues[index] = value.B;
    //            rgbValues[index + 1] = value.G;
    //            rgbValues[index + 2] = value.R;
    //            rgbValues[index + 3] = value.A;
    //        }
    //    }

    //    /// <summary>
    //    /// Width of the image. 
    //    /// </summary>
    //    public int Width
    //    {
    //        get
    //        {
    //            return image.Width;
    //        }
    //    }

    //    /// <summary>
    //    /// Height of the image. 
    //    /// </summary>
    //    public int Height
    //    {
    //        get
    //        {
    //            return image.Height;
    //        }
    //    }

    //    /// <summary>
    //    /// Returns the modified Bitmap. 
    //    /// </summary>
    //    public Bitmap asBitmap()
    //    {
    //        System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);
    //        return image;
    //    }
    //}
    class ImageChecker
    {
        public static Rectangle searchBitmap(Bitmap smallBmp, Bitmap bigBmp, double tolerance = 0.05)
        {
            BitmapData smallData =
              smallBmp.LockBits(new Rectangle(0, 0, smallBmp.Width, smallBmp.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            BitmapData bigData =
              bigBmp.LockBits(new Rectangle(0, 0, bigBmp.Width, bigBmp.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            int smallStride = smallData.Stride;
            int bigStride = bigData.Stride;

            int bigWidth = bigBmp.Width;
            int bigHeight = bigBmp.Height - smallBmp.Height + 1;
            int smallWidth = smallBmp.Width * 3;
            int smallHeight = smallBmp.Height;

            Rectangle location = CHECKFAILED;
            int margin = Convert.ToInt32(255.0 * tolerance);

            unsafe
            {
                byte* pSmall = (byte*)(void*)smallData.Scan0;
                byte* pBig = (byte*)(void*)bigData.Scan0;

                int smallOffset = smallStride - smallBmp.Width * 3;
                int bigOffset = bigStride - bigBmp.Width * 3;

                bool matchFound = true;

                for (int y = 0; y < bigHeight; y++)
                {
                    for (int x = 0; x < bigWidth; x++)
                    {
                        byte* pBigBackup = pBig;
                        byte* pSmallBackup = pSmall;

                        //Look for the small picture.
                        for (int i = 0; i < smallHeight; i++)
                        {
                            int j = 0;
                            matchFound = true;
                            for (j = 0; j < smallWidth; j++)
                            {
                                //With tolerance: pSmall value should be between margins.
                                int inf = pBig[0] - margin;
                                int sup = pBig[0] + margin;
                                if (sup < pSmall[0] || inf > pSmall[0])
                                {
                                    matchFound = false;
                                    break;
                                }

                                pBig++;
                                pSmall++;
                            }

                            if (!matchFound) break;

                            //We restore the pointers.
                            pSmall = pSmallBackup;
                            pBig = pBigBackup;

                            //Next rows of the small and big pictures.
                            pSmall += smallStride * (1 + i);
                            pBig += bigStride * (1 + i);
                        }

                        //If match found, we return.
                        if (matchFound)
                        {
                            location.X = x;
                            location.Y = y;
                            location.Width = smallBmp.Width;
                            location.Height = smallBmp.Height;
                            break;
                        }
                        //If no match found, we restore the pointers and continue.
                        else
                        {
                            pBig = pBigBackup;
                            pSmall = pSmallBackup;
                            pBig += 3;
                        }
                    }

                    if (matchFound) break;

                    pBig += bigOffset;
                }
            }

            bigBmp.UnlockBits(bigData);
            smallBmp.UnlockBits(smallData);

            return location;
        }

        public static Rectangle CHECKFAILED = new Rectangle(-1, -1, -1, -1);
    }

    //class ImageChecker
    //{
    //    private int offsetx = 0;
    //    private int offsety = 0;

    //    //private LockedFastImage big_image;
    //    //private LockedFastImage small_image;
    //    private Bitmap big_image;
    //    private Bitmap small_image;
    //    /// <summary>
    //    /// The time needed for last operation.
    //    /// </summary>
    //    public TimeSpan time_needed = new TimeSpan();

    //    /// <summary>
    //    /// Error return value.
    //    /// </summary>
    //    static public Point CHECKFAILED = new Point(-1, -1);

    //    /// <summary>
    //    /// Constructor of the ImageChecker
    //    /// </summary>
    //    /// <param name="big_image">The image containing the small image.</param>
    //    /// <param name="small_image">The image located in the big image.</param>
    //    public ImageChecker(Bitmap big_image, Bitmap small_image)
    //    {
    //        this.big_image = big_image;
    //        this.small_image = small_image;
    //        //this.big_image = new LockedFastImage(big_image);
    //        //this.small_image = new LockedFastImage(small_image);
    //    }

    //    //public ImageChecker(LockedFastImage big_image, LockedFastImage small_image)
    //    //{
    //    //    this.big_image = big_image;
    //    //    this.small_image = small_image;
    //    //}

    //    //public ImageChecker(LockedFastImage big_image, Bitmap small_image)
    //    //{
    //    //    this.big_image = big_image;
    //    //    this.small_image = new LockedFastImage(small_image);
    //    //}

    //    //public ImageChecker(Bitmap big_image, LockedFastImage small_image)
    //    //{
    //    //    this.big_image = new LockedFastImage(big_image);
    //    //    this.small_image = small_image;
    //    //}

    //    /// <summary>
    //    /// Returns the location of the small image in the big image. Returns CHECKFAILED if not found.
    //    /// </summary>
    //    /// <param name="x_speedUp">speeding up at x achsis.</param>
    //    /// <param name="y_speedUp">speeding up at y achsis.</param>
    //    /// <param name="begin_percent_x">Reduces the search rect. 0 - 100</param>
    //    /// <param name="end_percent_x">Reduces the search rect. 0 - 100</param>
    //    /// <param name="begin_percent_x">Reduces the search rect. 0 - 100</param>
    //    /// <param name="end_percent_y">Reduces the search rect. 0 - 100</param>
    //    public Point bigContainsSmall(int x_speedUp = 1, int y_speedUp = 1, int begin_percent_x = 0, int end_percent_x = 100, int begin_percent_y = 0, int end_percent_y = 100, float maxTolerance = 0.05f)
    //    {
    //        /*
    //         * SPEEDUP PARAMETER
    //         * It might be enough to check each second or third pixel in the small picture.
    //         * However... In most cases it would be enough to check 4 pixels of the small image for diablo porposes.
    //         * */

    //        /*
    //         * BEGIN, END PARAMETER
    //         * In most cases we know where the image is located, for this we have the begin and end paramenters.
    //         * */

    //        DateTime begin = DateTime.Now;

    //        if (x_speedUp < 1) x_speedUp = 1;
    //        if (y_speedUp < 1) y_speedUp = 1;
    //        if (begin_percent_x < 0 || begin_percent_x > 100) begin_percent_x = 0;
    //        if (begin_percent_y < 0 || begin_percent_y > 100) begin_percent_y = 0;
    //        if (end_percent_x < 0 || end_percent_x > 100) end_percent_x = 100;
    //        if (end_percent_y < 0 || end_percent_y > 100) end_percent_y = 100;

    //        int x_start = (int)((double)big_image.Width * ((double)begin_percent_x / 100.0));
    //        int x_end = (int)((double)big_image.Width * ((double)end_percent_x / 100.0));
    //        int y_start = (int)((double)big_image.Height * ((double)begin_percent_y / 100.0));
    //        int y_end = (int)((double)big_image.Height * ((double)end_percent_y / 100.0));
    //        int mTolerance = (int) ((small_image.Height / y_speedUp) * (small_image.Width / x_speedUp) * maxTolerance);

    //        /*
    //         * We cant speed up the big picture, because then we have to check pixels in the small picture equal to the speeded up size 
    //         * for each pixel in the big picture.
    //         * Would give no speed improvement.
    //         * */

    //        //+ 1 because first pixel is in picture. - small because image have to be fully in the other image
    //        for (int x = x_start; x < x_end - small_image.Width + 1; x++)
    //            for (int y = y_start; y < y_end - small_image.Height + 1; y++)
    //            {
    //                //now we check if all pixels matches
    //                int tolerance = 0;
    //                for (int sx = 0; sx < small_image.Width; sx += x_speedUp)
    //                    for (int sy = 0; sy < small_image.Height; sy += y_speedUp)
    //                    {
    //                        if (small_image.GetPixel(sx, sy) != big_image.GetPixel(x + sx, y + sy))
    //                            tolerance++;
    //                        if (tolerance >= mTolerance)
    //                            goto CheckFailed;
    //                    }

    //                //check ok
    //                time_needed = DateTime.Now - begin;
    //                return new Point(x + offsetx, y + offsety);

    //            CheckFailed: ;
    //            }

    //        time_needed = DateTime.Now - begin;
    //        return CHECKFAILED;
    //    }
    //}
}
