﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPBJ
{
    public partial class Form1 : Form
    {
        public static Form1 MainForm;
        Blackjack game;

        public Size size = Screen.PrimaryScreen.Bounds.Size;
        public Point origin = new Point(0, 0);

        // Images
        Bitmap doubleBitmap = Properties.Resources.doubleText;
        Bitmap hitBitmap = Properties.Resources.hitText;
        Bitmap splitBitmap = Properties.Resources.splitText;
        Bitmap standBitmap = Properties.Resources.standText;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            origin = Properties.Settings.Default.Origin;
            size = Properties.Settings.Default.Size;

            if (size.Width == 0 || size.Height == 0)
            {
                Properties.Settings.Default.Size = Screen.PrimaryScreen.Bounds.Size;
                Properties.Settings.Default.Save();
                size = Properties.Settings.Default.Size;
            }

            MainForm = this;
            game = new Blackjack(size, origin);
        }

        /// <summary>
        /// Handles the Click event of the BtnStart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BtnStart_Click(object sender, EventArgs e)
        {
            pbTimer.Maximum = game.getVoteTimer();
            GameUpdate.Start();
            ChatTimer.Start();
            
            game.start();
            
        }

        /// <summary>
        /// Handles the Tick event of the GameUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void GameUpdate_Tick(object sender, EventArgs e)
        {
            game.tick();
        }

        /// <summary>
        /// Handles the Tick event of the ChatTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ChatTimer_Tick(object sender, EventArgs e)
        {
            lblState.Text = game.state.ToString();
            game.chat();

            if (game.state == Blackjack.GameState.ACTION)
            {
                float hitValue = game.getVotesInPercent(0);
                float standValue = game.getVotesInPercent(1);
                float doubleValue = 0;
                if (game.canDouble)
                {
                    doubleValue = game.getVotesInPercent(2);
                }
                float splitValue = 0;
                if (game.canSplit)
                {
                    splitValue = game.getVotesInPercent(3);
                }
                float cap = 0;
                float max = Math.Max(Math.Max(splitValue, doubleValue), Math.Max(hitValue, standValue));
                if (max > 0)
                    cap = 100f / max;

                vpbHit.Value = (int)(game.getVotesInPercent(0) * cap);
                vpbStand.Value = (int)(game.getVotesInPercent(1) * cap);
                if (game.canDouble)
                {
                    vpbDouble.Value = (int)(game.getVotesInPercent(2) * cap);
                }
                else
                {
                    vpbDouble.Value = 0;
                }
                if (game.canSplit)
                {
                    vpbSplit.Value = (int)(game.getVotesInPercent(3) * cap);
                }
                else
                {
                    vpbSplit.Value = 0;
                }

                switch (game.getHighestVote((int)hitValue, (int)standValue, (int)doubleValue, (int)splitValue))
                {
                    case Blackjack.Action.HIT:
                        pictureBox1.Image = hitBitmap;
                        break;
                    case Blackjack.Action.STAND:
                        pictureBox1.Image = standBitmap;
                        break;
                    case Blackjack.Action.DOUBLE:
                        pictureBox1.Image = doubleBitmap;
                        break;
                    case Blackjack.Action.SPLIT:
                        pictureBox1.Image = splitBitmap;
                        break;
                    default:
                        break;
                }

                lblVotes.Text = "Votes: " + game.getVoteCount();
                int time = game.getTimer().Seconds;
                pbTimer.Value = time;
                lblTimer.Text = "Timer: " + time;
            }
            else
            {
                lblTimer.Text = "";
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        /// <summary>
        /// Handles the MouseDown event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            AreaSelect select = new AreaSelect();
            select.Show();
        }

        /// <summary>
        /// Handles the Tick event of the ImageTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ImageTimer_Tick(object sender, EventArgs e)
        {
            pictureBox2.Image = game.screenshot;
        }

        /// <summary>
        /// Updates the game.
        /// </summary>
        internal void updateGame()
        {
            game.update(size, origin);
        }

        /// <summary>
        /// Handles the Click event of the btnSetupIRC control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSetupIRC_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = Properties.Settings.Default.IRCPath;
            openFileDialog.Title = "Select the HexChat log file";
            openFileDialog.Filter = "log files (*.log)|*.log|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(openFileDialog.FileName))
                {
                    Properties.Settings.Default.IRCPath = openFileDialog.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }
    }
}
