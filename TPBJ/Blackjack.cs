﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPBJ.Ext;

namespace TPBJ
{

    class Blackjack
    {
        private DateTime targetTime;
        public bool canSplit = false;
        public bool canDouble = true;
        private bool isCalculating = false;
        Dictionary<string, byte> votes = new Dictionary<string, byte>();

        private Size size;
        private Point origin;

        //Images
        public Bitmap screenshot;
        Bitmap dealBitmap = Properties.Resources.deal;
        Bitmap doubleBitmap = Properties.Resources._double;
        Bitmap hitBitmap = Properties.Resources.hit;
        Bitmap loseBitmap = Properties.Resources.lose;
        Bitmap placeBetBitmap = Properties.Resources.placeBet;
        Bitmap splitBitmap = Properties.Resources.split;
        Bitmap standBitmap = Properties.Resources.stand;
        Bitmap waitingBitmap = Properties.Resources.waiting;
        Bitmap insBitmap = Properties.Resources.ins;
        Bitmap bet2Bitmap = Properties.Resources.bet2;

        // Timer
        private int voteTimer = 10;

        // Chat reader
        private ChatReader reader;

        /// <summary>
        /// Initializes a new instance of the <see cref="Blackjack"/> class.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <param name="origin">The origin.</param>
        public Blackjack(Size size, Point origin)
        {
            this.size = size;
            this.origin = origin;
            screenshot = new Bitmap(size.Width, size.Height);
            CaptureScreen();

            reader = new ChatReader(Properties.Settings.Default.IRCPath);
        }

        public GameState state = GameState.BET;
        public enum GameState
        {
            BET, ACTION, LOST, WAIT
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Action
        {
            HIT, STAND, DOUBLE, SPLIT
        }


        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void start()
        {
            bet();
        }

        /// <summary>
        /// Places bet.
        /// </summary>
        public void bet()
        {
            Rectangle result = Rectangle.Empty;
            do
            {
                result = ImageChecker.searchBitmap(placeBetBitmap, CaptureScreen());
                if (result == ImageChecker.CHECKFAILED)
                {
                    result = ImageChecker.searchBitmap(bet2Bitmap, CaptureScreen());
                }
            } while (result == ImageChecker.CHECKFAILED);
            MouseClick.LeftMouseClick(result, origin);
            deal();
        }

        /// <summary>
        /// Deals this instance.
        /// </summary>
        private void deal()
        {
            
            //ImageChecker dealCheck = new ImageChecker(CaptureScreen(), new Bitmap(@"c:\users\lasse\documents\visual studio 2013\Projects\TPBJ\TPBJ\Images\deal.PNG"));
            Rectangle deal = ImageChecker.CHECKFAILED;
            DateTime target = DateTime.Now.AddSeconds(5);
            do
            {
                deal = ImageChecker.searchBitmap(dealBitmap, CaptureScreen());
            } while (deal == ImageChecker.CHECKFAILED);

            MouseClick.LeftMouseClick(deal, origin);

            state = GameState.WAIT;
        }


        /// <summary>
        /// Switches to action.
        /// </summary>
        private void switchToAction()
        {
            state = GameState.ACTION;
            targetTime = DateTime.Now.AddSeconds(voteTimer);
            votes = new Dictionary<string, byte>();
            checkOptional();
        }

        /// <summary>
        /// Performs action if timer has run out.
        /// </summary>
        private void action()
        {
            if (DateTime.Now.CompareTo(targetTime) > 0)
            {
                clickOnAction(getHighestVote());
                int hitVotes = getVotes(0);
                int standVotes = getVotes(1);
                int doubleVotes = getVotes(2);
                int splitVotes = 0;
                if (canSplit)
                {
                    splitVotes = getVotes(3);
                }

                if (standVotes >= hitVotes && standVotes >= doubleVotes && standVotes >= splitVotes)
                {
                    clickOnAction(Action.STAND);
                }
                else if (hitVotes >= doubleVotes && hitVotes >= splitVotes)
                {
                    clickOnAction(Action.HIT);
                }
                else if (doubleVotes >= splitVotes)
                {
                    clickOnAction(Action.DOUBLE);
                }
                else
                {
                    clickOnAction(Action.SPLIT);
                }

                state = GameState.WAIT;
            }
        }

        /// <summary>
        /// Gets the highest vote.
        /// </summary>
        /// <returns>The vote</returns>
        private Action getHighestVote()
        {
            int hitVotes = getVotes(0);
            int standVotes = getVotes(1);
            int doubleVotes = getVotes(2);
            int splitVotes = 0;
            if (canSplit)
            {
                splitVotes = getVotes(3);
            }

            return getHighestVote(hitVotes, standVotes, doubleVotes, splitVotes);
        }

        /// <summary>
        /// Gets the highest vote.
        /// </summary>
        /// <param name="hitVotes">The hit votes.</param>
        /// <param name="standVotes">The stand votes.</param>
        /// <param name="doubleVotes">The double votes.</param>
        /// <param name="splitVotes">The split votes.</param>
        /// <returns>The vote</returns>
        public Action getHighestVote(int hitVotes, int standVotes, int doubleVotes, int splitVotes)
        {
            if (standVotes >= hitVotes && standVotes >= doubleVotes && standVotes >= splitVotes)
            {
                return Action.STAND;
            }
            else if (hitVotes >= doubleVotes && hitVotes >= splitVotes)
            {
                return Action.HIT;
            }
            else if (doubleVotes >= splitVotes)
            {
                return Action.DOUBLE;
            }
            else
            {
                return Action.SPLIT;
            }
        }

        /// <summary>
        /// Clicks on action.
        /// </summary>
        /// <param name="action">The action.</param>
        private void clickOnAction(Action action)
        {
            Bitmap image;
            switch (action)
            {
                case Action.HIT:
                    image = hitBitmap;
                    break;
                case Action.STAND:
                    image = standBitmap;
                    break;
                case Action.DOUBLE:
                    image = doubleBitmap;
                    break;
                case Action.SPLIT:
                    image = splitBitmap;
                    break;
                default:
                    image = standBitmap;
                    break;
            }

            Rectangle position = ImageChecker.searchBitmap(image, CaptureScreen());

            if (position != ImageChecker.CHECKFAILED)
            {
                MouseClick.LeftMouseClick(position, origin);
            }

            //image.Dispose();
        }

        /// <summary>
        /// Gets the votes.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>Count of votes</returns>
        private int getVotes(byte action)
        {
            return votes.Where(p => p.Value == action).Count();
        }

        /// <summary>
        /// Gets the votes in percent.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns>Votes in percent</returns>
        public float getVotesInPercent(byte action)
        {
            float allVotes = ((float)votes.Count);
            float specVotes = ((float)votes.Where(p => p.Value == action).Count());
            float value = 0;
            if (allVotes > 0)
                value = specVotes / allVotes * 100f;
            float min = Math.Min(100f, value);
            float max = Math.Max(0f, min);
            return max;
        }

        /// <summary>
        /// Checks the optional buttons.
        /// </summary>
        public void checkOptional()
        {
            Rectangle hit = ImageChecker.searchBitmap(hitBitmap, CaptureScreen());
            if (hit == ImageChecker.CHECKFAILED)
            {
                state = GameState.WAIT;
                canSplit = false;
                canDouble = false;
            }
            else
            {
                Rectangle split = ImageChecker.searchBitmap(splitBitmap, CaptureScreen());
                canSplit = split != ImageChecker.CHECKFAILED;
                Rectangle doubleC = ImageChecker.searchBitmap(doubleBitmap, CaptureScreen());
                canDouble = doubleC != ImageChecker.CHECKFAILED;
            }
        }

        /// <summary>
        /// Captures the screen.
        /// </summary>
        /// <returns>The captured image</returns>
        private Bitmap CaptureScreen()
        {
            using (Graphics graphics = Graphics.FromImage(screenshot))
            {
                graphics.CopyFromScreen(origin, Point.Empty, size);
                return screenshot;
            }
        }


        /// <summary>
        /// Ticks this instance.
        /// </summary>
        internal void tick()
        {
            if (!isCalculating)
            {
                isCalculating = true;
                switch (state)
                {
                    case GameState.BET:
                        break;
                    case GameState.ACTION:
                        action();
                        break;
                    case GameState.LOST:
                        checkLost();
                        break;
                    case GameState.WAIT:
                        wait();
                        break;
                    default:
                        break;
                }
                isCalculating = false;
            }
        }

        /// <summary>
        /// Checks  if lost and restarts.
        /// </summary>
        private void checkLost()
        {
            Rectangle lost = ImageChecker.searchBitmap(loseBitmap, CaptureScreen());
            if (lost != ImageChecker.CHECKFAILED)
            {
                MouseClick.LeftMouseClick(lost, origin);
                state = GameState.BET;
                bet();
            }
        }

        /// <summary>
        /// Waits until action required.
        /// </summary>
        private void wait()
        {
            Rectangle wait = ImageChecker.searchBitmap(hitBitmap, CaptureScreen());
            if (wait != ImageChecker.CHECKFAILED)
            {
                switchToAction();
            }
            else
            {
                Rectangle vbet = ImageChecker.searchBitmap(placeBetBitmap, CaptureScreen());
                if (vbet != ImageChecker.CHECKFAILED)
                {
                    state = GameState.BET;
                    bet();
                    return;
                }

                Rectangle ins = ImageChecker.searchBitmap(insBitmap, CaptureScreen());
                if (ins != ImageChecker.CHECKFAILED)
                {
                    MouseClick.LeftMouseClick(ins, origin);
                    return;
                }

                checkLost();
            }
        }

        /// <summary>
        /// Read votes from chat.
        /// </summary>
        internal void chat()
        {
            if (state == GameState.ACTION)
            {
                Queue<Command> newCommands = reader.getCommands();
                while (newCommands.Count > 0)
                {
                    Command cmd = newCommands.Dequeue();
                    switch (cmd.command.ToLower())
                    {
                        case "hit":
                            votes[cmd.name] = 0;
                            break;
                        case "stand":
                            votes[cmd.name] = 1;
                            break;
                        case "double":
                            votes[cmd.name] = 2;
                            break;
                        case "split":
                            votes[cmd.name] = 3;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the timer.
        /// </summary>
        /// <returns></returns>
        internal TimeSpan getTimer()
        {
            return targetTime.Subtract(DateTime.Now);
        }

        /// <summary>
        /// Gets the vote count.
        /// </summary>
        /// <returns></returns>
        internal int getVoteCount()
        {
            return votes.Count();
        }

        internal void update(Size size, Point origin)
        {
            this.origin = origin;
            this.size = size;
            screenshot = new Bitmap(size.Width, size.Height);
            CaptureScreen();
        }

        /// <summary>
        /// Gets the vote timer.
        /// </summary>
        /// <returns></returns>
        internal int getVoteTimer()
        {
            return voteTimer;
        }
    }
}
