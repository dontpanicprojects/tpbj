﻿using ChatSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPBJ
{
    /// <summary>
    /// Reads commands from a HexChat twitch chat log file
    /// </summary>
    class ChatReader
    {
        private Queue<Command> commands = new Queue<Command>();
        private DateTime lastChecked = DateTime.Now;
        private String path = "";

        // Regex string
        private string findComment = @"(^| )!(\w+)";// Old regex: @"<(.+)>	.*!(\w+)";


        /// <summary>
        /// Initializes a new instance of the <see cref="ChatReader"/> class.
        /// </summary>
        /// <param name="pathToLogFile">The path to log file.</param>
        public ChatReader(String pathToLogFile)
        {
            //path = pathToLogFile;
            var client = new IrcClient("irc.twitch.tv", new IrcUser("o0xenira0o", "o0xenira0o", "oauth:q3scjqkxmk0g5ja2ftu0glwbj7hf00"));
            client.ConnectionComplete += (s, e) => client.JoinChannel("#o0xenira0o");
            client.UserMessageRecieved += (s, e) =>
            {
                Console.WriteLine("<< {0}", e.PrivateMessage.User.User + ": " + e.PrivateMessage.Message);
                Match m = Regex.Match(e.PrivateMessage.Message, findComment);
                if (m.Success)
                {
                    commands.Enqueue(new Command(e.PrivateMessage.User.User, m.Groups[2].Value));
                }
            };
            client.ConnectAsync();
        }

        /// <summary>
        /// Gets the commands.
        /// </summary>
        /// <returns></returns>
        public Queue<Command> getCommands()
        {
            DateTime end = DateTime.Now.AddSeconds(1);
            Queue<Command> result = new Queue<Command>();
            while (commands.Count > 0 && end.CompareTo(DateTime.Now) >= 0)
            {
                result.Enqueue(commands.Dequeue());
            }
            //if (File.Exists(path))
            //{
            //    var lines = File.ReadLines(path);
            //    foreach (string s in lines) {
            //        Match m = Regex.Match(s, findComment);
            //        if (m.Success) {
            //            result.Enqueue(new Command(m.Groups[1].Value, m.Groups[2].Value));
            //        }
            //    }

            //    clearFile();
            //}

            //lastChecked = DateTime.Now;
            return result;
        }

        /// <summary>
        /// Clears the file.
        /// </summary>
        public void clearFile()
        {
            if (File.Exists(path))
            {
                var fileStream = File.Open(path, FileMode.Open);
                fileStream.SetLength(0);
                fileStream.Close();
            }
        }
    }

    /// <summary>
    /// Command made by chat
    /// </summary>
    public class Command
    {
        public string name;
        public string command;

        public Command(string name, string command)
        {
            this.name = name;
            this.command = command;
        }
    }
}
