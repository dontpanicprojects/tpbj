﻿namespace TPBJ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblState = new System.Windows.Forms.Label();
            this.pbTimer = new System.Windows.Forms.ProgressBar();
            this.lblTimer = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.GameUpdate = new System.Windows.Forms.Timer(this.components);
            this.ChatTimer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVotes = new System.Windows.Forms.Label();
            this.btnSetSize = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ImageTimer = new System.Windows.Forms.Timer(this.components);
            this.btnSetupIRC = new System.Windows.Forms.Button();
            this.vpbDouble = new TPBJ.VerticalProgressBar();
            this.vpbStand = new TPBJ.VerticalProgressBar();
            this.vpbHit = new TPBJ.VerticalProgressBar();
            this.vpbSplit = new TPBJ.VerticalProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(222, 9);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(50, 13);
            this.lblState.TabIndex = 5;
            this.lblState.Text = "Startup...";
            // 
            // pbTimer
            // 
            this.pbTimer.Location = new System.Drawing.Point(11, 159);
            this.pbTimer.Name = "pbTimer";
            this.pbTimer.Size = new System.Drawing.Size(261, 23);
            this.pbTimer.TabIndex = 6;
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(12, 185);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(61, 13);
            this.lblTimer.TabIndex = 7;
            this.lblTimer.Text = "Vote Timer:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(11, 201);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Split";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Hit";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Stand";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(110, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Double";
            // 
            // GameUpdate
            // 
            this.GameUpdate.Interval = 1000;
            this.GameUpdate.Tick += new System.EventHandler(this.GameUpdate_Tick);
            // 
            // ChatTimer
            // 
            this.ChatTimer.Tick += new System.EventHandler(this.ChatTimer_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(147, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // lblVotes
            // 
            this.lblVotes.AutoSize = true;
            this.lblVotes.Location = new System.Drawing.Point(148, 9);
            this.lblVotes.Name = "lblVotes";
            this.lblVotes.Size = new System.Drawing.Size(0, 13);
            this.lblVotes.TabIndex = 14;
            // 
            // btnSetSize
            // 
            this.btnSetSize.Location = new System.Drawing.Point(197, 201);
            this.btnSetSize.Name = "btnSetSize";
            this.btnSetSize.Size = new System.Drawing.Size(75, 23);
            this.btnSetSize.TabIndex = 15;
            this.btnSetSize.Text = "Setup";
            this.btnSetSize.UseVisualStyleBackColor = true;
            this.btnSetSize.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(11, 230);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(261, 107);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // ImageTimer
            // 
            this.ImageTimer.Enabled = true;
            this.ImageTimer.Interval = 2000;
            this.ImageTimer.Tick += new System.EventHandler(this.ImageTimer_Tick);
            // 
            // btnSetupIRC
            // 
            this.btnSetupIRC.Location = new System.Drawing.Point(101, 201);
            this.btnSetupIRC.Name = "btnSetupIRC";
            this.btnSetupIRC.Size = new System.Drawing.Size(75, 23);
            this.btnSetupIRC.TabIndex = 17;
            this.btnSetupIRC.Text = "IRC";
            this.btnSetupIRC.UseVisualStyleBackColor = true;
            this.btnSetupIRC.Click += new System.EventHandler(this.btnSetupIRC_Click);
            // 
            // vpbDouble
            // 
            this.vpbDouble.Location = new System.Drawing.Point(113, 12);
            this.vpbDouble.Name = "vpbDouble";
            this.vpbDouble.Size = new System.Drawing.Size(28, 128);
            this.vpbDouble.TabIndex = 4;
            // 
            // vpbStand
            // 
            this.vpbStand.Location = new System.Drawing.Point(79, 12);
            this.vpbStand.Name = "vpbStand";
            this.vpbStand.Size = new System.Drawing.Size(28, 128);
            this.vpbStand.TabIndex = 3;
            // 
            // vpbHit
            // 
            this.vpbHit.Location = new System.Drawing.Point(45, 12);
            this.vpbHit.Name = "vpbHit";
            this.vpbHit.Size = new System.Drawing.Size(28, 128);
            this.vpbHit.TabIndex = 2;
            // 
            // vpbSplit
            // 
            this.vpbSplit.Location = new System.Drawing.Point(11, 12);
            this.vpbSplit.Name = "vpbSplit";
            this.vpbSplit.Size = new System.Drawing.Size(28, 128);
            this.vpbSplit.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(284, 349);
            this.Controls.Add(this.btnSetupIRC);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnSetSize);
            this.Controls.Add(this.lblVotes);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.pbTimer);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.vpbDouble);
            this.Controls.Add(this.vpbStand);
            this.Controls.Add(this.vpbHit);
            this.Controls.Add(this.vpbSplit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VerticalProgressBar vpbSplit;
        private VerticalProgressBar vpbHit;
        private VerticalProgressBar vpbStand;
        private VerticalProgressBar vpbDouble;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.ProgressBar pbTimer;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer GameUpdate;
        private System.Windows.Forms.Timer ChatTimer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblVotes;
        private System.Windows.Forms.Button btnSetSize;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer ImageTimer;
        private System.Windows.Forms.Button btnSetupIRC;

    }
}

